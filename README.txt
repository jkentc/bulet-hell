Welcome to Invading Space!
This is a 1-round bullet hell game where you have to destroy the alien spaceship and take over their ship!
Try to evade all of the laser beams that's coming to you!

You will win when you destroyed all of alien spaceship and weapons.
You will lose when you are the one who get destroyed!

When you get hit, You'll get a 0.5 Seconds Invulnerability from ALL ATTACKS.

There are 2 Types of Weapon!
• Single Shot
• Burst Shot

Controls:
W/A/S/D: Movement
Space: Shoot
P: Pause Game
Q: Switch Weapon Type


That's it! Enjoy!

Gitlab Link
https://gitlab.com/jkentc/bulet-hell