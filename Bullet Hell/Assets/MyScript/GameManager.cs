﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int _win = 0;
    private int _enemycounter;
    void update()
    {
        
    }
    private void EndRound()
    {
        
    }

    public void Restart()
    {
        print("Restart called!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void GameStart()
    {
        print("GameStart called!");
        SceneManager.LoadScene("GamePlay");
    }
    public void MainMenu()
    {
        print("MainMenu called!");
        SceneManager.LoadScene("MainMenu");
    }

}
