﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class UiManager : MonoBehaviour
{
    //Ui GameObject
    private static GameObject _canvas;
    private static GameObject _playingUI;
    private static GameObject _pauseUI;
    private static GameObject _endRoundUI;
    //Variable
    public static int _score = 0;

    // Start is called before the first frame update
    void Start()
    {
        //find Canvas in the game
        _canvas = GameObject.Find("UI_Grp");
        //find UI childs
        _playingUI = _canvas.transform.Find("PlayingUI").gameObject;
        _playingUI.SetActive(true);
        _pauseUI = _canvas.transform.Find("PauseUI").gameObject;
        _pauseUI.SetActive(false);
        _endRoundUI = _canvas.transform.Find("EndRoundUI").gameObject;
        _endRoundUI.SetActive(false);

        //setting score to 0 in start of the game
        _score = 0;
        SetScore();
        //setting hp value in start of the game
        SetHp();

        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && PlayerScript.PlayerAlive)
        {
            Pause();
        }
        CheckPlayerStatus();
    }

    //function to add score that can be called by other gameobject/script
    public static void AddScore(int score)
    {
        _score += score;
        SetScore();
    }

    //function to set the text on the ui
    private static void SetScore()
    {
        //find the child in gameobject PlayingUI and set the score in the tmp text component
        _playingUI.transform.Find("Score").GetComponent<TMP_Text>().text = "Score: " + _score;
    }
    public static void SetHp()
    {
        _playingUI.transform.Find("HP").GetComponent<TMP_Text>().text = "HP: " + PlayerScript._hp;
    }
    void Pause()
    {
        if (Time.timeScale == 1)
        {
            ShowPaused();
        }
        else if (Time.timeScale == 0)
        {
            HidePaused();
        }
    }

    //2 function for game pause
    void ShowPaused()
    {
        Time.timeScale = 0;
        _pauseUI.SetActive(true);
        _pauseUI.transform.Find("Score").GetComponent<TMP_Text>().text = "Score: " + _score;
        _playingUI.SetActive(false);
    }
    void HidePaused()
    {
        Time.timeScale = 1;
        _pauseUI.SetActive(false);
        _playingUI.SetActive(true);
    }
    void CheckPlayerStatus()
    {
        if (!PlayerScript.PlayerAlive || EnemyScript.EnemyCount == 0)
        {
            _playingUI.SetActive(false);
            _endRoundUI.SetActive(true);
            SetEndRoundScreen();
        }
    }
    void SetEndRoundScreen()
    {
        _endRoundUI.transform.Find("Score").GetComponent<TMP_Text>().text = "Score: " + _score;
        _endRoundUI.transform.Find("GameOverMSG").GetComponent<TMP_Text>().text = (PlayerScript.PlayerAlive) ? "You Win !!" : "You Lose !!";
        Time.timeScale = 0;

    }
}
