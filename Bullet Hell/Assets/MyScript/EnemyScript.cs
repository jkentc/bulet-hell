﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    //GameObjects
    [SerializeField]
    public GameObject _projectile;
    public GameObject _target;


    // [SerializeField]
    // private KeyCode _shootKey = KeyCode.Space;

    //enemy setting
    [SerializeField]
    public float _hp = 3;
    public float _delayShoot = 1.5f;
    public float _delayShootTime;
    private Vector3 _curPos; //current position
    [SerializeField]
    private float _movementSpeed = 2;
    private float _moveSpeed;
    private Rigidbody2D _rigidBody;
    private Vector2 _vel; //velocity
    [SerializeField]
    private bool _enableMovement = true;
    [SerializeField]
    private int _type = 1;

    //projectile setting
    [SerializeField]
    private float _projectileSpeed = 8f;

    //enemy counter
    public static int EnemyCount { get; private set; }

    private void OnDestroy()
    {
        EnemyCount--;
    }

    // Start is called before the first frame update
    void Start()
    {
        EnemyCount++;
        //setting player as the target
        _target = GameObject.Find("Player"); //set player as the target
        _delayShootTime = Time.time + Random.Range(300, 600) / 100; //prevent enemy from shooting immediately
        _curPos = gameObject.transform.position; //get current position
        _rigidBody = gameObject.GetComponent<Rigidbody2D>();
        _vel.x = (Random.Range(0, 2) == 1) ? 1 : -1; //random the 1st vel value
        _moveSpeed = (Random.Range(1, (_movementSpeed + 1) * 100) / 100); //random the movement speed of the ship
    }

    // Update is called once per frame
    void Update()
    {
        ChekDead();
        LookAt(_target);

        if (_enableMovement) Move();
        if (_type == 1)
        {
            AutoShoot();
        }
        else if (_type == 2)
        {
            AutoShoot2();
        }
    }

    //check if hit by player's bullet
    void OnTriggerEnter2D(Collider2D other)
    {
        //check if the obj is a projectile
        if (other.tag == "Projectile")
        {
            //if projectile fired by player ship decrease the hp
            if (other.gameObject.GetComponent<Projectile>()._originShiptag == "Player") _hp--;
        }
    }

    //function that rotate the gameobject facing target
    private void LookAt(GameObject target)
    {
        //creating roatation using vector calculation
        //source https://answers.unity.com/questions/705524/how-do-i-make-an-object-look-at-an-another-object.html
        if (PlayerScript.PlayerAlive)
        {
            var dir = target.transform.position - transform.position;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
        }
    }

    //function that check for hp and destroy object
    private void ChekDead()
    {
        if (_hp <= 0)
        {
            UiManager.AddScore(1);
            Destroy(gameObject);
        }
    }

    //function that makes the enemy shoot with delay
    private void AutoShoot()
    {
        //giving delay to enemy shooting mechanic
        if (Time.time > _delayShootTime)
        {
            Shoot();
        }
    }
    private void AutoShoot2()
    {
        //giving delay to enemy shooting mechanic
        if (Time.time > _delayShootTime)
        {
            Shoot(-45);
            Shoot(-22.5f);
            Shoot();
            Shoot(22.5f);
            Shoot(45);
        }
    }
    private void Shoot(float deg = 0)
    {
        _delayShootTime = Time.time + _delayShoot;
        //create a projectile variable as gameobject at the position of the ship with rotation matching ship rotation
        GameObject bullet = (GameObject)Instantiate(_projectile, transform.position, transform.rotation * Quaternion.Euler(0f, 0f, deg));
        bullet.GetComponent<Projectile>().firing_ship = gameObject;
        bullet.GetComponent<Rigidbody2D>().velocity = bullet.transform.up * _projectileSpeed;
    }

    private void Move()
    {
        //check if thegame object position is between set limit
        //the ternary operator simmilar to the beario animation
        _vel.x = (gameObject.transform.position.x > _curPos.x + 2) ? -1 : (gameObject.transform.position.x < _curPos.x - 2) ? 1 : _vel.x;

        //the ternary operator catains the if below
        // if (gameObject.transform.position.x > _curPos.x +2) _vel.x = -1;
        // else if (gameObject.transform.position.x < _curPos.x -2) _vel.x = 1;

        //set the velocity
        _vel.y = 0; //vel y is 0 becasue i only want the enemy to move sideways
        _rigidBody.velocity = new Vector2(_vel.x * _moveSpeed, _vel.y * _moveSpeed);
    }
}
