﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this script is based on https://www.youtube.com/watch?v=ailbszpt_AI
public class Boundaries : MonoBehaviour
{
    private Vector2 _screenBounds;
    private float _objectWidth;
    private float _objectHeight;

    // Start is called before the first frame update
    void Start()
    {
        _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        _objectWidth = transform.GetComponent<SpriteRenderer>().bounds.size.x / 2;
        _objectHeight = transform.GetComponent<SpriteRenderer>().bounds.size.y / 2;    
    }

    void LateUpdate()
    {
        Vector3 _viewPos = transform.position;
        //due to update this part of the script need to be adusted from the original
        _viewPos.x = Mathf.Clamp(_viewPos.x, _screenBounds.x * -1 + _objectWidth, _screenBounds.x -_objectWidth);
        _viewPos.y = Mathf.Clamp(_viewPos.y, _screenBounds.y * -1 + _objectHeight, _screenBounds.y -_objectHeight);
        transform.position = _viewPos;  
    }
}
