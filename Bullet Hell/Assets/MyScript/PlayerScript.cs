﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerScript : MonoBehaviour
{
    //GameObjects
    [SerializeField]
    public GameObject _projectile;
    [SerializeField]
    public GameObject _projectile2;

    //player setting
    [SerializeField]
    public static int _hp = 5;
    [SerializeField]
    public float _invTimer = 0.5f; //player invulnerable timer
    private float _invTime = 0; //time stamp to help with invulnerable mechanic

    //player movement speed setting
    [SerializeField]
    private float _moveSpeed = 1;
    private Rigidbody2D _rigidBody;

    //player movement inputs
    private float _xInput;
    [SerializeField]
    private KeyCode _shootKey = KeyCode.Space;

    //projectile setting
    [SerializeField]
    private float _projectileSpeed = 8f;
    private int _projectileType = 0;
    [SerializeField]
    private float _projectileDelay = 0.2f;
    [SerializeField]
    private float _projectile2Delay = 0.2f;
    private float _projectileTime;

    //alive detector
    public static bool PlayerAlive { get; private set; }
    private void OnDestroy()
    {
        PlayerAlive = false;
    }


    void Awake()
    {
        PlayerAlive = true;
        _rigidBody = GetComponent<Rigidbody2D>();
        _projectileTime = Time.time;
        _projectileType = 0;
        _hp = 15;
    }

    // Update is called once per frame
    void Update()
    {
        CheckDeath();
        ReadMoveInput();
        ReadShootInput();
    }

    private void ReadMoveInput()
    {
        //setting velocity based on horizontal and vertical input
        _rigidBody.velocity = new Vector2(Input.GetAxis("Horizontal") * _moveSpeed, Input.GetAxis("Vertical") * _moveSpeed);
    }

    private void ReadShootInput()
    {
        //instantiate bulelt
        _projectileType = (Input.GetKeyDown(KeyCode.Q)) ? (_projectileType == 0) ? 1 : 0 : _projectileType; //change weapon type by pressing q
        if (Input.GetKey(_shootKey) && Time.time > _projectileTime)
        {
            //create a projectile variable as gameobject at the position of the ship without rotation

            if (_projectileType == 0)
            {
                SpanwBullet();
            }
            else
            {
                //spawn the bullet 3 times with diferent angle
                SpanwBullet(22.5f);
                SpanwBullet();
                SpanwBullet(-22.5f);
            }

        }
    }
    //spawn bullet inth degree rotation in z axis
    private void SpanwBullet(float deg = 0)
    {
        GameObject bullet;
        bullet = (GameObject)Instantiate(_projectile, transform.position, Quaternion.Euler(0f, 0f, deg));
        _projectileTime = (_projectileType == 0) ? Time.time + _projectileDelay : Time.time + _projectile2Delay; //setting time delay based on projectile type
        bullet.GetComponent<Projectile>().firing_ship = gameObject;
        bullet.GetComponent<Rigidbody2D>().velocity = bullet.transform.up * _projectileSpeed;
    }

    private void CheckDeath()
    {
        if (_hp <= 0) die();
    }

    public void die()
    {
        Destroy(gameObject);
    }

    //check if hit by enemy's bullet
    void OnTriggerEnter2D(Collider2D other)
    {
        //check if the obj is a projectile
        if (other.tag == "Projectile")
        {
            //if projectile fired by non player ship decrease the hp
            if (other.gameObject.GetComponent<Projectile>()._originShiptag != "Player")
            {
                //check if player is invulnerable
                if (_invTime <= Time.time)
                {
                    _hp--;
                    UiManager.SetHp();
                    MakeInv(); //make player invulnerable after getting hit
                }
            }
        }
    }

    //set the invulnerable player invulnerable time window
    void MakeInv()
    {
        _invTime = Time.time + _invTimer;
    }

}
