﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomMovePath : MonoBehaviour
{
    [SerializeField] Transform pointA;
    [SerializeField] Transform pointB;

    [SerializeField] float movementTime;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (pointA && pointB)
        {
            Gizmos.DrawLine(pointA.position, pointB.position);
        }

        Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, Vector3.one * 0.3f);
    }

    private void Update()
    {
        transform.position = Vector3.Lerp(pointA.position, pointB.position, (Mathf.Sin(Time.time) + 1) / 2f);
    }
}
